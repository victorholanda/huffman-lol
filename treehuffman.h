#pragma once

#include <QList>
#include <QHash>
#include <nodehuff.h>
#include <QString>
class TreeHuffman {
    NodeHuff * root;
    QHash<uchar,QString> hash;
    QByteArray repTree;
    int sizeTrash;
    QString sizeName;
    int indice;
    QByteArray header;
public:
    TreeHuffman();
    TreeHuffman(QList<NodeHuff*> list);
    NodeHuff * get() const;
    QHash<uchar,QString> getHash();
    QString getRep();
    QByteArray getHeader();
    void show(NodeHuff* node, int level = 0);
    void coding(NodeHuff* node,QString temp = "");
    void buildRep(NodeHuff* node);
    void buildHeader(QString path);
    void createHeader(QString path, int trash);
    void setSizeTrash(int value);
    NodeHuff * rebuild(QByteArray rep);

};
