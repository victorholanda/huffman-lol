#pragma once

#include <QString>
#include <QObject>
class FileCompress : public QObject{
    Q_OBJECT
public:
    FileCompress(QObject *parent = 0);
    Q_INVOKABLE static void compress(QString path, QString outPath = "");
    Q_INVOKABLE static void decompress(QString path, QString newDir = "");
};

