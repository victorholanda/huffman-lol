#include <QFile>
#include <QDebug>
#include <QByteArray>
#include "huffinfo.h"
#include "treehuffman.h"
#include "util.h"

QString HuffInfo::getOutDir() const{
    return outDir;
}

void HuffInfo::setOutDir(const QString &value){
    outDir = value;
}

QString HuffInfo::getName() const{
    return name;
}

QByteArray HuffInfo::getTree() const{
    return tree;
}

QBitArray HuffInfo::getCode() const{
    return code;
}

void HuffInfo::extract(QByteArray byte){
    QBitArray partOne = Util::byteToBit(byte.at(0));
    QBitArray bitsTrash(3);
    bitsTrash.setBit(0,partOne.at(0));
    bitsTrash.setBit(1,partOne.at(1));
    bitsTrash.setBit(2,partOne.at(2));
    partOne.setBit(0,false);
    partOne.setBit(1,false);
    partOne.setBit(2,false);
    QBitArray partTwo = Util::byteToBit(byte.at(1));
    partOne.resize(16);
    for (int var = 8; var < 16; ++var)
        partOne.setBit(var,partTwo.at(var - 8));
    sizeTrash = Util::bitArrayToString(bitsTrash);
    sizeTree = Util::bitArrayToString(partOne);
}

HuffInfo::HuffInfo(){}

HuffInfo::HuffInfo(QString path){
    HuffInfo();
    QFile in(path);
    QByteArray content;
    if(in.open(QIODevice::ReadOnly))
        content = in.readAll();
    extract(content.left(2));
    sizeName = content.at(2);
    content.remove(0,3);
    name = content.left(sizeName);
    content.remove(0,sizeName);
    tree = content.left(sizeTree);
    content.remove(0,sizeTree);
    code.resize(content.size()*8 - sizeTrash);
    code.fill(false);
    for (int var = 0, i = 0,j = 0; i < content.size(); ++var){
        code.setBit(var,Util::byteToBit(content.at(i)).at(j));
        if(j == 7){
            ++i;
            j=0;
        }else ++j;
        if(i == (content.size() - 1) && j == (8 - sizeTrash)) break;
    }
}

void HuffInfo::test(){
//    char c(123);
    qDebug() << int(0x21);
}
