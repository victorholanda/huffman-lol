#include "filecompress.h"
#include "treehuffman.h"
#include "util.h"
#include <QFile>
#include <QDebug>
#include <QTime>
#include <QFileInfo>
#include "huffinfo.h"

FileCompress::FileCompress(QObject *parent) : QObject(parent){}

void FileCompress::compress(QString path,QString outPath){
    if(outPath == "")
        outPath = QFileInfo(path).absolutePath() + '/'
                + QFileInfo(path).baseName() + ".huff";
    QTime t;
    t.start();
    TreeHuffman * tree = new TreeHuffman(Util::count(path));
    tree->coding(tree->get());
    int trash = Util::fileTemp(path,tree->getHash());
    tree->createHeader(path,trash);
    Util::headerTemp(path,tree->getHeader(), outPath);
    qDebug("Tempo de compressão: %d ms",t.elapsed());
}

void FileCompress::decompress(QString path,QString newDir){
    QTime t;
    t.start();
    HuffInfo fileInfo(path);
    if(newDir == "")
        newDir = QFileInfo(path).absolutePath()  + '/'+ fileInfo.getName();
    else newDir.append(fileInfo.getName());
    TreeHuffman tree;
    NodeHuff* root = tree.rebuild(fileInfo.getTree());
    QByteArray data = Util::restoreData(fileInfo.getCode(),root);
    Util::write(data,newDir);
    qDebug("Tempo de descompressão: %d ms",t.elapsed());
}
