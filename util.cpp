#include "util.h"
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QByteArray>
#include <QtAlgorithms>
#include <QUrl>
#include <QDebug>
#include <QBitArray>
#include <bits/stdc++.h>
#include <QtMath>
#include <QDataStream>

using namespace std;

bool Util::comp(const NodeHuff* a,const NodeHuff* b){
    if(a->getFreq() == b->getFreq())
        return a->getC() < b->getC();
    return a->getFreq() < b->getFreq();
}
int Util::fileTemp(QString path, QHash<uchar, QString> hash){
    QFile in(path);
    QFile out("huff.temp");
    QString str = "";
    int lixo = 0;
    if(in.open(QIODevice::ReadOnly) && out.open(QIODevice::WriteOnly)){
        QByteArray bytes = in.readAll();
        for (QByteArray::iterator it = bytes.begin(); it != bytes.end(); ++it)
            str += hash.value(*it);
        lixo = 8 - (str.length()%8);
        if(str.length() % 8 != 0)
            str += QString("0").repeated(lixo);
        out.write(toBinaryString(str));
    }
    return lixo;
}
bool Util::headerTemp(QString path, QByteArray header,QString outName){
    if(outName == ""){
        if(path.contains('.'))
            path.chop(path.length() - path.lastIndexOf('.'));
        path.append(".huff");
    }/*else{
        path.chop(path.length() - path.lastIndexOf('/'));
        path.append('/' + outName);
    }*/
    qDebug() << path << header << outName;
    QFile out(outName);
    QFile in("huff.temp");
    if(out.open(QIODevice::WriteOnly) && in.open(QIODevice::ReadOnly)){
        QByteArray content = in.readAll();
        header += content;
        out.write(header);

    }
    return out.size() > 0;
}

QList<NodeHuff *> Util::toQList(int * array){
    QList<NodeHuff*> list ;
    for (int i = 0; i < 256; ++i) {
        if(array[i] != 0)
            list << new NodeHuff(array[i],i);
    }
    qSort(list.begin(),list.end(),comp);
    return list;
}
QList<NodeHuff*> Util::count(QString path){
    int count[256] = {0};
    QFile *file = new QFile(path);
    if(file->open(QIODevice::ReadOnly)){
        QByteArray array = file->readAll();
        QByteArray::iterator i;
        for (i = array.begin(); i != array.end(); ++i)
            count[uchar(*i)]++;
    }
    file->close();
    return toQList(count);
}

QBitArray Util::byteToBit(char byte){
    QBitArray bit(8);
    bit.setBit(7, 0x80&byte);
    bit.setBit(6, 0x40&byte);
    bit.setBit(5, 0x20&byte);
    bit.setBit(4, 0x10&byte);
    bit.setBit(3, 0x08&byte);
    bit.setBit(2, 0x04&byte);
    bit.setBit(1, 0x02&byte);
    bit.setBit(0, 0x01&byte);
    return bit;
}

int Util::bitArrayToString(const QBitArray array ){
    int size = array.count();
    int result = 0;
    for (int var = 0,aux = size - 1; var < size; ++var, --aux) {
        result += array.at(var) * qPow(2,aux);
    }
    return result;
}

QBitArray Util::joinQBitArray(QBitArray a, QBitArray b){
    QDataStream out;
    out << a;
    out << b;
    QBitArray result;
    out >> result;
    return result;
}

QByteArray Util::restoreData(QBitArray array, NodeHuff *root){
    QByteArray result;
    NodeHuff* curr = root;
    for (int var = 0; var < array.count(); ++var) {
        if(array.at(var)){
            curr = curr->getRight();
        }else{
            curr = curr->getLeft();
        }if(curr->isLeaf()){
            result.append(curr->getC());
            curr = root;
        }
    }
    return result;
}

void Util::write(QByteArray in, QString path){
//    qDebug() << in << path;
    QFile file(path);
    if(file.open(QIODevice::WriteOnly)){
        file.write(in);
    }
}

QByteArray Util::toBinaryString(QString str){
    QByteArray result;
    while(str.size())    {
        QString byte = str.left(8); str.remove(0,8);
        reverse(byte.begin(),byte.end());
        char aux = byte.toInt(0,2);
        result.append(aux);
    }
    return result;
}
