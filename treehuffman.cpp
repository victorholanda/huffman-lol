#include <QDebug>
#include <QFile>
#include <QIODevice>
#include <QBitArray>
#include "treehuffman.h"
#include "bitarray.h"
#include "util.h"
#include <QFileInfo>
#include <QStack>
#include <QChar>
#include <QtAlgorithms>
#include <bits/stdc++.h>
using namespace std;

void TreeHuffman::setSizeTrash(int value){
    sizeTrash = value;
}

NodeHuff *TreeHuffman::rebuild(QByteArray rep){
    root =  new NodeHuff((uchar)'*');
    QStack<NodeHuff*> stack;
    for (int var = rep.length() - 1; var > 0; --var) {
        char curr = rep.at(var);
        if(curr == '*'){
            if(rep.at(var-1) == '!' && var){
                stack.push(new NodeHuff((uchar)curr));
                --var;
            } else {
                NodeHuff* right = stack.pop();
                NodeHuff* left = stack.pop();
                NodeHuff* aux = new NodeHuff(left, right);
                stack.push(aux);
            }
        } else if(curr == '!') {
            stack.push(new NodeHuff((uchar)curr));
            --var;
        } else stack.push(new NodeHuff((uchar)curr));
    }
    NodeHuff* right = stack.pop();
    NodeHuff* left = stack.pop();
    NodeHuff* aux = new NodeHuff(left, right);
    stack.push(aux);
    root = stack.pop();
    return root;
}

TreeHuffman::TreeHuffman(){}

TreeHuffman::TreeHuffman(QList<NodeHuff *> list){
    while(list.length() > 2){
        NodeHuff* temp = new NodeHuff(list.at(0)->getFreq()
                                      + list.at(1)->getFreq() , false);
        temp->set(list.at(0), list.at(1));
        list.removeFirst();
        list.removeFirst();
        list.insert(0,temp);
        qSort(list.begin(),list.end(),Util::comp);
    }
    if(list.length() == 2){
        root = new NodeHuff(list.at(0)->getFreq()
                            + list.at(1)->getFreq(), false);
        root->set(list.at(0), list.at(1));
        list.clear();
    }
    else{
        qDebug() << Q_FUNC_INFO << "Fail build tree";
    }
}
void TreeHuffman::show(NodeHuff *node, int level){
    if(node != 0){
        show(node->getRight(), level + 1);
        if(node->isLeaf())
            qDebug() << qPrintable(QString("\t").repeated(level))
                     << uchar(node->getC()) << "/" << node->getFreq();
        else
            qDebug() << qPrintable(QString("\t").repeated(level))
                     << char(46);
        show(node->getLeft(), level + 1);
    }
}
NodeHuff * TreeHuffman::get() const{
    return root;
}
void TreeHuffman::coding(NodeHuff *node, QString temp){
    if(node != 0){
        if(node->isLeaf()){
            hash.insert(node->getC(),temp);
        }
        else{
            temp += '1';
            coding(node->getLeft(), temp);
            temp.chop(1);
            temp += '0';
            coding(node->getRight(), temp);
        }
    }
}
QHash<uchar,QString> TreeHuffman::getHash(){
    if(root != 0)
        coding(root);
    return hash;
}
void TreeHuffman::buildRep(NodeHuff *node){
    if(node != 0){
        if(!node->isLeaf()){
            repTree.append('*');
            buildRep(node->getLeft());
            buildRep(node->getRight());
        }else{
            if(node->getC() == '!' || node->getC() == '*'){
                repTree.append('!');
            }
            repTree.append(node->getC());
        }
    }
}
QString TreeHuffman::getRep(){
    if(root != 0)
        buildRep(root);
    return repTree;
}
void TreeHuffman::createHeader(QString path,int trash){
    buildRep(root);
    QString trashStr;
    QString treeLength;
    if (trash == 8) trash = 0;
    trashStr.setNum(trash,2);
    treeLength.setNum(repTree.length(),2);
    if(trashStr.length() != 3)
        trashStr.insert(0,QString('0').repeated(3 - trashStr.length()));
    if(treeLength.length() != 13)
        treeLength.insert(0,QString('0').repeated(13 - treeLength.length()));
    QByteArray firstBytes =  Util::toBinaryString(trashStr + treeLength);
    header.append(firstBytes);
    header.append(char(QFileInfo(path).fileName().length()));
    header.append(QFileInfo(path).fileName());
    header.append(repTree);
}
QByteArray TreeHuffman::getHeader(){
    return header;
}
