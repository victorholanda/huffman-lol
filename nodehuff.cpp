#include <nodehuff.h>

NodeHuff *NodeHuff::getRight() const{
    return right;
}
NodeHuff *NodeHuff::getLeft() const{
    return left;
}
bool NodeHuff::isLeaf() const{
    return left == 0 && right == 0;
}
void NodeHuff::setLeft(NodeHuff *value){
    left = value;
}

void NodeHuff::setRight(NodeHuff *value){
    right = value;
}

void NodeHuff::setC(const unsigned char &value){
    c = value;
}

NodeHuff::NodeHuff(unsigned char c){
    this->c = c;
    this->left = 0;
    this->right = 0;
}

NodeHuff::NodeHuff(NodeHuff *left, NodeHuff *right){
    this->left = left;
    this->right = right;

}

NodeHuff::NodeHuff(unsigned int freq,unsigned char c){
    this->c = c;
    this->freq = freq;
    this->left = 0;
    this->right = 0;
}
unsigned int NodeHuff::getFreq() const{
    return freq;
}
uchar NodeHuff::getC() const{
    return c;
}
QString NodeHuff::toString(){
    return QString("value: ") + QString(c) + QString(" count: ")  + freq;
}
void NodeHuff::set(NodeHuff* left,NodeHuff* right){
    this->left = left;
    this->right = right;
}
