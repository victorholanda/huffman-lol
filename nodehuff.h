#pragma once

#include <QString>
#include <QObject>

class NodeHuff{
    unsigned char c;
    unsigned int freq = 0;
    NodeHuff * left;
    NodeHuff * right;
public:
    NodeHuff(unsigned char c);
    NodeHuff(NodeHuff* left, NodeHuff* right);
    NodeHuff(unsigned int freq, unsigned char c = 0);
    unsigned int getFreq() const;
    uchar getC() const;
    NodeHuff *getRight() const;
    NodeHuff *getLeft() const;
    bool isLeaf() const;
    QString toString();
    void set(NodeHuff*left,NodeHuff*right);
    void setLeft(NodeHuff *value);
    void setRight(NodeHuff *value);
    void setC(const uchar &value);
};
