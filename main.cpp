#include <QDebug>
#include <QTime>
#include <filecompress.h>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QCommandLineParser>
#include "util.h"
#include "huffinfo.h"

int main(int argc, char *argv[]){
    QTime t;
    t.start();
    //START APP
    QApplication app(argc,argv);
    QApplication::setApplicationName("Huffman");
    QApplication::setApplicationVersion("0.0");
    QQmlApplicationEngine engine;
    QQmlContext *interpreter = engine.rootContext();
    FileCompress huff(&app);
    interpreter->setContextProperty("_huffman",&huff);
    QCommandLineParser parser;
    //Parser
    parser.addHelpOption();
    parser.addVersionOption();
    parser.setApplicationDescription("Huffman Parsers");
    parser.addPositionalArgument("in-file.x", QCoreApplication::translate("main", "File being compressed."));
    parser.addPositionalArgument("out-name.huff", QCoreApplication::translate("main", "Name to save archive."));
    parser.addPositionalArgument("dir", QCoreApplication::translate("main", "Dir being compressed"));
    parser.addPositionalArgument("local", QCoreApplication::translate("main", "Local to save archive."));
    QCommandLineOption compress("c",QApplication::translate("main","Compress <in-file.x>."),
                                QApplication::translate("main","in-file.x"));
    parser.addOption(compress);
    QCommandLineOption outName("o",QApplication::translate("main","Save as <out-name.huff>."),
                               QApplication::translate("main","out-file.huff"));
    parser.addOption(outName);
    QCommandLineOption local("d",QApplication::translate("main","Decompress in <local>."),
                             QApplication::translate("main","local"));
    parser.addOption(local);
    QCommandLineOption startGui({"g", "gui"},QApplication::translate("main","Start gui."));
    parser.addOption(startGui);
    parser.process(app);
    //ARGUMENTS
    if(parser.isSet(startGui)){
        engine.load(QUrl(QStringLiteral("qrc:/ui.qml")));
        return app.exec();
    }
    else if(parser.isSet(compress) && parser.isSet(outName))
        FileCompress::compress(parser.value(compress),parser.value(outName));
    else if(parser.isSet(compress))
        FileCompress::compress(parser.value(compress));
    else{
        if(app.arguments().size() == 1)
            qDebug() << qPrintable(parser.helpText());
        else if(parser.isSet(local))
            FileCompress::decompress(app.arguments().at(1),parser.value(local));
        else
            FileCompress::decompress(app.arguments().at(1));
    }
    //END APP
//    qDebug("%s <> Tempo de execução: %d ms",Q_FUNC_INFO,t.elapsed());

}
